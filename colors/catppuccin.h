 static const char *colors[SchemeLast][2] = {
 	/*                  fg         bg       */
	[SchemeNorm] = { "#cdd6f4", "#1a1826" },
	[SchemeSel] = { "#1a1826", "#f5c2e7" },
	[SchemeOut] = { "#cdd6f4", "#1a1826" },
 };
