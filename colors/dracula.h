 static const char *colors[SchemeLast][2] = {
 	/*                  fg         bg       */
	[SchemeNorm] = { "#f8f8f2", "#282a36" },
	[SchemeSel] = { "#282a36", "#8be9fd" },
	[SchemeOut] = { "#f8f8f2", "#282a36" },
 };
