 static const char *colors[SchemeLast][2] = {
 	/*                  fg         bg       */
	[SchemeNorm] = { "#abb2bf", "#282c34" },
	[SchemeSel] = { "#282c34", "#56b6c2" },
	[SchemeOut] = { "#abb2bf", "#e06c75" },
 };
