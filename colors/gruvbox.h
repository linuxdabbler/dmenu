 static const char *colors[SchemeLast][2] = {
 	/*     fg         bg       */
	[SchemeNorm] = { "#ebdbb2", "#222526" },
	[SchemeSel] = { "#1d2021", "#98971a" },
	[SchemeOut] = { "#ebdbb2", "#8ec07c" },
 };
