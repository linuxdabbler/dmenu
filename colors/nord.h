 static const char *colors[SchemeLast][2] = {
 	/*                  fg         bg       */
	[SchemeNorm] = { "#eceff4", "#2e3440" },
	[SchemeSel] = { "#2e3440", "#5e81ac" },
	[SchemeOut] = { "#eceff4", "#2e3440" },
 };
